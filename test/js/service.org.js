angular.module('myApp', ["ngResource"])
    // コントローラー
    .controller('MyController', ["$scope", "$resource", "$filter", function ($scope, $resource, $filter) {

        var $scope;


        // JSONファイルから情報を取得

        var SrvList = $resource("./json/items_list.json");
        $scope.basServices = SrvList.get(function (data) {
            $scope.bb_basic = data.bb_basic;
            $scope.nsp = data.nsp;
            $scope.nsp_hw = data.nsp_hw;
            $scope.bb_next_new = data.bb_next_new;
            $scope.bb_next_rep = data.bb_next_rep;
            $scope.bb_std_new = data.bb_std_new;
            $scope.bb_std_rep = data.bb_std_rep;
            $scope.provider = data.provider;
            $scope.provider_new = data.provider_new;
            $scope.pcantivirus = data.pcantivirus;
            $scope.pcantivirus_nsp = data.pcantivirus_nsp;
            $scope.hosting = data.hosting;
            $scope.sslip = data.sslip;
            $scope.domain = data.domain;
            $scope.mail = data.mail;
            $scope.siteblock = data.siteblock;
            $scope.vpn = data.vpn;
            $scope.vpn_option1 = data.vpn_option1;
            $scope.vpn_option2 = data.vpn_option2;
            $scope.maintainance_bas = data.maintainance_bas;
            $scope.maintainance_std = data.maintainance_std;
            $scope.wifi = data.wifi;
            $scope.lan = data.lan;
        });

        // 契約形態
        $scope.cont_rep = 'リプレース';
        $scope.cont_new = '新規';

        // 既契約パネルの表示フラグ
        $scope.precont_show = true;
        $scope.precont2op_show = true;
        $scope.newcont_show = true;
        $scope.nspop_show = true;
        $scope.bbnop_show = true;
        $scope.bbsop_show = true;

        // 契約台数のセット
        $scope.contractNums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
        $scope.contractNumber = 1;
        $scope.ispMailNums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
        $scope.vpnOp2Nums = [1, 2, 3];
        $scope.landevNums = [1, 2, 3, 4, 5]

        $scope.oldBasSum = 0;
        $scope.oldOptSum = 0;

        // 既契約の初期値設定
        $scope.contSel = {
            vpcsec: 'なし',
            sbl: 'なし',
            host: 'なし',
            ssla: 'なし',
            doma: 'なし',
            ispmail: 'なし',
            ispmail_nums: 1,
            maintain_bas: 'なし',
            maintain_std: 'なし',
            wlan: 'なし',
            vpnsv: 'なし',
            vpnop1: 'なし',
            vpnop2: 'なし',
            vpnop2_nums: 1,
            landev1: 'なし',
            landev1Nums: 1,
            landev2: 'なし',
            landev2Nums: 1,
            landev3: 'なし',
            landev3Nums: 1,
            landev4: 'なし',
            landev4Nums: 1,
            landev5: 'なし',
            landev5Nums: 1,
            landev6: 'なし',
            landev6Nums: 1,
            landev7: 'なし'
        };

        $scope.newCont = {
            contractNumber: '',
            nspRep: {
                'svname': '',
                'initial': {
                    'code': '',
                    'price': ''
                },
                'monthly': {
                    'code': '',
                    'price': ''
                }
            },
            newPv: {
                'svname': '',
                'initial': {
                    'code': '',
                    'price': ''
                },
                'monthly': {
                    'code': '',
                    'price': ''
                }
            },
            nspvpcsec: {
                'svname': '',
                'initial': {
                    'code': '',
                    'price': ''
                },
                'monthly': {
                    'code': '',
                    'price': ''
                }
            },
            nspmaintain: '',
            maintain_std: '',
            vpn: {
                'svname': '',
                'initial': {
                    'code': '',
                    'price': 0
                }
            },
            vpn_option1: {
                'svname': '',
                'initial': {
                    'code': '',
                    'price': 0
                }
            },
            vpn_option2: {
                'nums': 0,
                'svname': '',
                'initial': {
                    'code': '',
                    'price': 0,
                    'sumprice': 0
                },
                'monthly': {
                    'code': '',
                    'price': 0,
                    'sumprice': 0
                }
            },
            landev1: {
                'nums': 0,
                'svname': '',
                'initial': {
                    'code': '',
                    'price': 0,
                    'sumprice': 0
                }
            },
            landev2: {
                'nums': 0,
                'svname': '',
                'initial': {
                    'code': '',
                    'price': 0,
                    'sumprice': 0
                }
            },
            landev3: {
                'nums': 0,
                'svname': '',
                'initial': {
                    'code': '',
                    'price': 0,
                    'sumprice': 0
                }
            },
            landev4: {
                'nums': 0,
                'svname': '',
                'initial': {
                    'code': '',
                    'price': 0,
                    'sumprice': 0
                }
            },
            landev5: {
                'nums': 0,
                'svname': '',
                'initial': {
                    'code': '',
                    'price': 0,
                    'sumprice': 0
                }
            },
            landev6: {
                'nums': 0,
                'svname': '',
                'initial': {
                    'code': '',
                    'price': 0,
                    'sumprice': 0
                }
            },
            landev7: {
                'svname': '',
                'initial': {
                    'code': '',
                    'price': 0,
                },
                'monthly': {
                    'code': '',
                    'price': 0,
                }
            }

        };

        $scope.precont = {
            contractNumber: '',
            oldSv: '',
            oldPv: '',
            newPv: '',
            vpcsec: '',
            sbl: '',
            host: '',
            ssla: '',
            doma: '',
            ispmail: '',
            maintain_bas: '',
            wlan: ''
        };


        // サービス名からサービス情報を取得
        function getSvname(obj, prop, id_svname) {
            if (prop == 'pcnums') {
                return ($filter("filter")(obj, { pcnums: id_svname }, true))[0];
            } else {
                return ($filter("filter")(obj, { svname: id_svname }, true))[0];
            }
        };


        // 旧契約の計算
        function recalcPrecont() {
            $scope.oldBasSum = $scope.precont.oldSv.monthly.price + $scope.precont.oldPv.monthly.price;
            $scope.oldOptSum = $scope.precont.vpcsec.monthly.sumprice
                + $scope.precont.host.monthly.price
                + $scope.precont.ssla.monthly.price
                + $scope.precont.doma.monthly.price
                + $scope.precont.ispmail.monthly.sumprice
                + $scope.precont.maintain_bas.monthly.price
                + $scope.precont.wlan.monthly.price;
            $scope.oldTotalSum = $scope.oldBasSum + $scope.oldOptSum;
        };

        /* 共通オプションの計算
           ホスティング、SSL、ドメイン管理、プロバイダメール、LAN構築の合計金額を出す
        */
        // 初期費用
        function recalcBasInitOpts() {
            return ($scope.newCont.host.initial.price
                + $scope.newCont.ssla.initial.price
                + $scope.newCont.doma.initial.price
                + $scope.newCont.ispmail.initial.sumprice
                + $scope.newCont.landevSum);
        };

        // 月額費用
        function recalcBasMonthlyOpts() {
            return ($scope.newCont.host.monthly.price
                + $scope.newCont.ssla.monthly.price
                + $scope.newCont.doma.monthly.price
                + $scope.newCont.ispmail.monthly.sumprice);
        };

        // NSP契約の計算
        /*
          NSP契約の計算
           ・NSPはUPSハードウェア代を初期費用に含む
                   ・ NSPは独自のPCウィルス対策契約がある
          ・保守時間延長、無線LANはオプションサービスには含まない
        */
        function recalcNSPNewcont() {
            // 初期費用の計算
            var BasInitOpts, BasMonthlyOpts;
            $scope.newNspBasInitSum =
                $scope.newCont.nspRep.initial.price
                + $scope.newCont.newPv.initial.price
                + $scope.nsp_hw.initial.price;

            BasInitOpts = recalcBasInitOpts();
            $scope.newCont.nspvpcsec.initial.sumprice = $scope.newCont.nspvpcsec.initial.price * $scope.newCont.contractNumber;


            $scope.newNspOptInitSum = BasInitOpts
                + $scope.newCont.nspvpcsec.initial.sumprice;

            $scope.newNspInitSum = $scope.newNspBasInitSum + $scope.newNspOptInitSum;

            // 月額費用の計算
            $scope.newNspBasMonthlySum =
                $scope.newCont.nspRep.monthly.price
                + $scope.newCont.newPv.monthly.price;

            BasMonthlyOpts = recalcBasMonthlyOpts();
            $scope.newCont.nspvpcsec.monthly.sumprice = $scope.newCont.nspvpcsec.monthly.price * $scope.newCont.contractNumber;

            $scope.newNspOptMonthlySum = BasMonthlyOpts
                + $scope.newCont.nspvpcsec.monthly.sumprice;

            $scope.newNspMonthlySum = $scope.newNspBasMonthlySum + $scope.newNspOptMonthlySum;
        };

        // BBパック Next
        /*
           BBパック Nextの計算
           ・BBパックNextはVPN構築を含む
           ・VPNはハブ、スポークが選択可能
           */

        function recalcBBNextNewcont() {
            var BasInitOpts, BasMonthlyOpts;

            // 初期費用の計算
            // 初期費用は乗換と新規で異なる
//            console.log($scope.cont_rep);
//            console.log($scope.cont_new);
//            console.log($scope.cont_stat);
            if ($scope.cont_stat == $scope.cont_rep) {
                $scope.newBBNextBasInitSum =
                    $scope.newCont.bbnextRep.initial.price + $scope.newCont.newPv.initial.price;
            } else {
                $scope.newBBNextBasInitSum =
                    $scope.newCont.bbnextNew.initial.price + $scope.newCont.newPv.initial.price;
            }

            BasInitOpts = recalcBasInitOpts();
            $scope.newCont.vpcsec.initial.sumprice = $scope.newCont.vpcsec.initial.price * $scope.newCont.contractNumber;

            $scope.newBBNextOptInitSum = BasInitOpts
                + $scope.newCont.vpcsec.initial.sumprice
                + $scope.newCont.vpn.initial.price
                + $scope.newCont.vpn_option1.initial.price
                + $scope.newCont.landev7.initial.price;

            $scope.newBBNextInitSum = $scope.newBBNextBasInitSum + $scope.newBBNextOptInitSum;

            // 月額費用の計算
            $scope.newBBNextBasMonthlySum = $scope.newCont.bbnextRep.monthly.price
                + $scope.newCont.newPv.monthly.price;

            BasMonthlyOpts = recalcBasMonthlyOpts();
            $scope.newCont.vpcsec.monthly.sumprice = $scope.newCont.vpcsec.monthly.price * $scope.newCont.contractNumber;

            $scope.newBBNextOptMonthlySum = BasMonthlyOpts
                + $scope.newCont.vpcsec.monthly.sumprice
                + $scope.newCont.vpn.monthly.price
                + $scope.newCont.landev7.monthly.price;

            $scope.newBBNextMonthlySum = $scope.newBBNextBasMonthlySum + $scope.newBBNextOptMonthlySum;

        };

        // BBパック　スタンダード２
        /*
           BBパック スタンダード２の計算
           ・BBパックスタンダード２はVPN構築（初期費用のみ）を含む
           ・VPNはリモートアクセス設定が選択可能
           */
        function recalcBBStdNewcont() {
            var BasInitOpts, BasMonthlyOpts;
            // 初期費用の計算
            // 初期費用は乗換と新規で異なる
            if ($scope.cont_stat == $scope.cont_rep) {
                $scope.newBBStdBasInitSum =
                    $scope.newCont.bbstdRep.initial.price + $scope.newCont.newPv.initial.price;
            } else {
                $scope.newBBStdBasInitSum =
                    $scope.newCont.bbstdNew.initial.price + $scope.newCont.newPv.initial.price;
            }



            BasInitOpts = recalcBasInitOpts();
            $scope.newCont.vpcsec.initial.sumprice = $scope.newCont.vpcsec.initial.price * $scope.newCont.contractNumber;
            $scope.newCont.sbl.initial.sumprice = $scope.newCont.sbl.initial.price * $scope.newCont.contractNumber;


            $scope.newBBStdOptInitSum = BasInitOpts
                + $scope.newCont.vpcsec.initial.sumprice
                + $scope.newCont.sbl.initial.sumprice
                + $scope.newCont.vpn.initial.price
                + $scope.newCont.vpn_option1.initial.price
                + $scope.newCont.vpn_option2.initial.sumprice
                + $scope.newCont.maintain_std.initial.price;

            $scope.newBBStdInitSum = $scope.newBBStdBasInitSum + $scope.newBBStdOptInitSum;

            // 月額費用の計算
            $scope.newBBstdBasMonthlySum = $scope.newCont.bbstdRep.monthly.price
                + $scope.newCont.newPv.monthly.price;

            BasMonthlyOpts = recalcBasMonthlyOpts();
            $scope.newCont.vpcsec.monthly.sumprice = $scope.newCont.vpcsec.monthly.price * $scope.newCont.contractNumber;
            $scope.newCont.sbl.monthly.sumprice = $scope.newCont.sbl.monthly.price * $scope.newCont.contractNumber;

            $scope.newBBstdOptMonthlySum = BasMonthlyOpts
                + $scope.newCont.maintain_std.monthly.price
                + $scope.newCont.vpcsec.monthly.sumprice
                + $scope.newCont.sbl.monthly.sumprice
                + $scope.newCont.vpn.monthly.price
                + $scope.newCont.vpn_option2.monthly.sumprice;

            $scope.newBBstdMonthlySum = $scope.newBBstdBasMonthlySum + $scope.newBBstdOptMonthlySum;
        };


        function recalcNewcont() {
            // NSP
            recalcNSPNewcont();

            // BBパック Next
            recalcBBNextNewcont();

            // BBパック　スタンダード２
            recalcBBStdNewcont();
        };

        // 計算の開始
        $scope.$watchCollection('contSel', function (newValue, oldValue, scope) {
            // 旧契約情報の抜き出し
            //  契約PC台数
            scope.precont.contractNumber = scope.contSel.contractNumber;

            // 基本サービス
            if (!scope.contSel.oldSv) {
                scope.precont.oldSv = getSvname(scope.bb_basic, 'pcnums', scope.precont.contractNumber);
            }

            // プロバイダ
            scope.precont.oldPv = (!scope.contSel.oldPv || scope.contSel.oldPv.svname == 'なし') ? scope.contSel.oldPv : getSvname(scope.provider, 'svname', scope.contSel.oldPv);

            // PCセキュリティ
            scope.precont.vpcsec = getSvname(scope.pcantivirus, 'svname', scope.contSel.vpcsec);
            scope.precont.vpcsec.monthly.sumprice = scope.precont.vpcsec.monthly.price * scope.contSel.contractNumber;

            // サイトブロック
            scope.precont.sbl = getSvname(scope.siteblock, 'svname', scope.contSel.sbl);
            scope.precont.sbl.monthly.sumprice = scope.precont.sbl.monthly.price * scope.contSel.contractNumber;

            // ホスティング
            scope.precont.host = getSvname(scope.hosting, 'svname', scope.contSel.host);

            // SSL IPアドレス
            scope.precont.ssla = getSvname(scope.sslip, 'svname', scope.contSel.ssla);

            //ドメイン
            scope.precont.doma = getSvname(scope.domain, 'svname', scope.contSel.doma);

            // プロバイダメール
            scope.precont.ispmail = getSvname(scope.mail, 'svname', scope.contSel.ispmail);
            scope.precont.ispmail.nums = scope.contSel.ispmail_nums;
            scope.precont.ispmail.initial.sumprice = scope.precont.ispmail.initial.price * scope.precont.ispmail.nums;
            scope.precont.ispmail.monthly.sumprice = scope.precont.ispmail.monthly.price * scope.precont.ispmail.nums;

            // 延長保守時間
            scope.precont.maintain_bas = getSvname(scope.maintainance_bas, 'svname', scope.contSel.maintain_bas);
            scope.precont.maintain_std = getSvname(scope.maintainance_std, 'svname', scope.contSel.maintain_std);

            // 無線LAN
            scope.precont.wlan = getSvname(scope.wifi, 'svname', scope.contSel.wlan);


            // 新契約情報の抜き出し
            scope.newCont.contractNumber = scope.precont.contractNumber;
            scope.newCont.nspRep = getSvname(scope.nsp, 'pcnums', scope.precont.contractNumber);

            scope.newCont.bbnextRep = getSvname(scope.bb_next_rep, 'pcnums', scope.precont.contractNumber);
            scope.newCont.bbnextNew = getSvname(scope.bb_next_new, 'pcnums', scope.precont.contractNumber);

            scope.newCont.bbstdRep = getSvname(scope.bb_std_rep, 'pcnums', scope.precont.contractNumber);
            scope.newCont.bbstdNew = getSvname(scope.bb_std_new, 'pcnums', scope.precont.contractNumber);

            scope.newCont.newPv = getSvname(scope.provider, 'svname', scope.precont.oldPv.new_sv);
            scope.newCont.vpcsec = scope.precont.vpcsec;
            (!(scope.newCont.vpcsec.svname == '' || scope.newCont.vpcsec.svname == 'なし')) ?
                scope.newCont.nspvpcsec = getSvname(scope.pcantivirus_nsp, 'svname', 'ネットワークセキュリティパック PCウイルス対策') : scope.newCont.nspvpcsec = scope.newCont.vpcsec;
            scope.newCont.sbl = scope.precont.sbl;
            scope.newCont.host = scope.precont.host;
            scope.newCont.ssla = scope.precont.ssla;
            scope.newCont.doma = scope.precont.doma;
            scope.newCont.ispmail = scope.precont.ispmail;
            scope.newCont.maintain_std = (!scope.precont.maintain_bas || scope.precont.maintain_bas.svname == 'なし') ? scope.precont.maintain_std : getSvname(scope.maintainance_std, 'svname', scope.precont.maintain_bas.new_sv);

            /*
                        scope.newCont.maintain_std = (!scope.precont.maintain_bas || scope.precont.maintain_bas.svname == 'なし') ? scope.precont.maintain_bas : getSvname(scope.maintain_std, 'svname',scope.precont.maintain_bas.new_sv);
                        (!(scope.precont.maintain_bas == '' || scope.precont.maintain_bas == 'なし')) ?
                            scope.newCont.nspmaintain.svname = scope.precont.maintain_bas.svname + 'は本商品ではご契約いただけません。（サービスに含まれています）' : scope.newCont.nspmaintain = scope.precont.maintain_bas;
            */
            // VPN
            //  VPNハブ以外では、VPN設計・リモートアクセスは選択できない
            //　ハブ以外が選ばれたら、VPN設計・リモートアクセスは「なし」にリセット
            if (!(scope.contSel.vpnsv == 'VPNハブ')) {
                scope.contSel.vpnop1 = 'なし';
                scope.contSel.vpnop2 = 'なし';
                scope.contSel.vpnop2_nums = 1;
            };

            //  VPNハブは固定IPが必須のため、プロバイダ選択で固定IPが選択されていない場合は、警告を表示
            scope.fixedip = scope.contSel.oldPv.match('固定');
                 
            scope.newCont.vpn = getSvname(scope.vpn, 'svname', scope.contSel.vpnsv);
            scope.newCont.vpn_option1 = getSvname(scope.vpn_option1, 'svname', scope.contSel.vpnop1);
            scope.newCont.vpn_option2 = getSvname(scope.vpn_option2, 'svname', scope.contSel.vpnop2);
            scope.newCont.vpn_option2.nums = scope.contSel.vpnop2_nums;
            scope.newCont.vpn_option2.initial.sumprice = scope.newCont.vpn_option2.initial.price * scope.newCont.vpn_option2.nums;
            scope.newCont.vpn_option2.monthly.sumprice = scope.newCont.vpn_option2.monthly.price * scope.newCont.vpn_option2.nums;

            // LAN構築
            scope.newCont.landevSum = 0;

            scope.newCont.landev1 = getSvname(scope.lan, 'svname', scope.contSel.landev1);
            scope.newCont.landev1.nums = scope.contSel.landev1Nums;
            scope.newCont.landev1.initial.sumprice = scope.newCont.landev1.initial.price * scope.newCont.landev1.nums;
            scope.newCont.landevSum += scope.newCont.landev1.initial.sumprice;

            scope.newCont.landev2 = getSvname(scope.lan, 'svname', scope.contSel.landev2);
            scope.newCont.landev2.nums = scope.contSel.landev2Nums;
            scope.newCont.landev2.initial.sumprice = scope.newCont.landev2.initial.price * scope.newCont.landev2.nums;
            scope.newCont.landevSum += scope.newCont.landev2.initial.sumprice;

            scope.newCont.landev3 = getSvname(scope.lan, 'svname', scope.contSel.landev3);
            scope.newCont.landev3.nums = scope.contSel.landev3Nums;
            scope.newCont.landev3.initial.sumprice = scope.newCont.landev3.initial.price * scope.newCont.landev3.nums;
            scope.newCont.landevSum += scope.newCont.landev3.initial.sumprice;

            scope.newCont.landev4 = getSvname(scope.lan, 'svname', scope.contSel.landev4);
            scope.newCont.landev4.nums = scope.contSel.landev4Nums;
            scope.newCont.landev4.initial.sumprice = scope.newCont.landev4.initial.price * scope.newCont.landev4.nums;
            scope.newCont.landevSum += scope.newCont.landev4.initial.sumprice;

            scope.newCont.landev5 = getSvname(scope.lan, 'svname', scope.contSel.landev5);
            scope.newCont.landev5.nums = scope.contSel.landev5Nums;
            scope.newCont.landev5.initial.sumprice = scope.newCont.landev5.initial.price * scope.newCont.landev5.nums;
            scope.newCont.landevSum += scope.newCont.landev5.initial.sumprice;

            scope.newCont.landev6 = getSvname(scope.lan, 'svname', scope.contSel.landev6);
            scope.newCont.landev6.nums = scope.contSel.landev6Nums;
            scope.newCont.landev6.initial.sumprice = scope.newCont.landev6.initial.price * scope.newCont.landev6.nums;
            scope.newCont.landevSum += scope.newCont.landev6.initial.sumprice;

            // 無線LAN　APはBBパック NEXTのみ（ここで合計しない）
            scope.newCont.landev7 = getSvname(scope.lan, 'svname', scope.contSel.landev7);


            // 旧契約の計算
            recalcPrecont();

            // 新契約の計算
            recalcNewcont();

        });
    }])

