angular.module('myApp', ["ngResource"])
    // コントローラー
    .controller('MyController', ["$scope", "$resource", "$filter", function ($scope, $resource, $filter) {

        var $scope;


        // JSONファイルから情報を取得
        // 様々な情報
        var Datas = $resource("./json/datas.json");
        $scope.datas = Datas.get(function (data) {
            $scope.home_types = data.home_type;
            $scope.servicers = data.servicer;
            $scope.line_types = data.line;
            $scope.isp_types = data.isp;
            $scope.tel_types = data.tel;
            $scope.hikari_ops = data.hikari_ops;
            $scope.trance_consts = data.trance_const;
            $scope.netprodcuts = data.netprodcuts;
            $scope.hikari_const = data.hikari_const;
        });


        // 既契約パネルの表示フラグ
        $scope.precont_show = true;
        $scope.precont2op_show = true;
        $scope.newcont_show = true;
        $scope.nspop_show = true;
        $scope.bbnop_show = true;
        $scope.bbsop_show = true;

        // 既契約の初期値設定
        $scope.contSel = {
            rwari: 'no',
            clName: ''
        };

        $scope.curentCont = {
            Line: "",
            Isp: "",
            Tel: ""
        };

        $scope.contNew = {
            Line: "",
            Isp: "",
            Tel: "",
            Line_repOps: 'repair_basic',
            Line_constType: 'no_dispatch',
            Line_constDay: 'disp_weekday',
            Line_corpTime: 'corp_day'
        };





        $scope.newCont = {
            line: {
                svId: "", svname: "", buildType: "", convType: "", menuType: "", monthlyPrice: "", repairName: "", repairPrice: "",
                constName: "", constCost: "", constDay: "", costTime: "", corpSupType: "", corpSupTimeCost: "", corpSupWe: "", corpSupWeCost: ""
            },
            isp: {
                svId: "", svname: "", monthlyPrice: ""
            },
            tel: {
                svId: "", svname: "", telType: "", monthlyPrice: "", nums: 0, numsCost: "", chs: 0, chsCost: "",
                tranceName: "", tranceCost: "", addMenuCost: "", c4Cost: "", c8Cost: "", options: { gd:"", nd:"", nr:"", vw:"", vw_nums:"", ro:"", rm:"", rm_nums:"", fm:"", fm_nums:"", rp:"" }
            }
        };

        $scope.estmate = {
            line: {

            }
        };

        // ひかり電話の有無
        $scope.hikariChkFlg = false;


        // 初期値設定
        //        $scope.newCont.Line.repOps = $scope.newCont.Line.repair_ops[0];

        // 転用の判定フィルター
        $scope.tranceFilter = function (value, index) {

        };



        function setMenuType() {
            // 既存ネットワーク契約の判定
            console.log('Check Line type');
            console.log('curentCont.Line:%o', $scope.contSel.Line)
            if ($scope.curentCont.Line) {
                // 転用もしくはBBパック　SELECTの場合は、アクセスメニュー
                if ($scope.curentCont.Line.conv_type == 'trance' || $scope.contSel.ri_netsv == 'bbs') {
                    $scope.contNew.menu_type = 'acc';
                };

                // 新規の場合はスタンダードメニュー
                if ($scope.curentCont.Line.conv_type == 'new') {
                    $scope.contNew.menu_type = 'std';
                };

                // 既存契約がリコーひかりのスタンダードメニューの場合はスタンダードメニュー
                if ($scope.curentCont.Line.servicer_id == 'rj' && $scope.contSel.Line.menu_type == 'std') {
                    $scope.contNew.menu_type = 'std';
                };
            };

            console.log('contNew.menu_type:%o', $scope.contNew.menu_type);


        };

        function setLineCont() {
            $scope.newCont.line.svId = $scope.contNew.Line.sv_id;
            $scope.newCont.line.svname = $scope.contNew.Line.svname;
            $scope.newCont.line.buildType = $scope.contSel.build_type;
            $scope.newCont.line.convType = $scope.contNew.Line.conv_type;
            $scope.newCont.line.menuType = $scope.contNew.Line.menu_type;
            $scope.newCont.line.monthlyPrice = $scope.contNew.Line.monthly_price;
            $scope.newCont.line.repairName = $scope.contNew.Line.repair_ops[$scope.contNew.Line_repOps].svname;
            $scope.newCont.line.repairPrice = $scope.contNew.Line.repair_ops[$scope.contNew.Line_repOps].monthly_price;
            $scope.newCont.line.constName = $scope.contNew.Line.construction[$scope.contNew.Line_constDay].constType;
            $scope.newCont.line.constCost = $scope.contNew.Line.construction[$scope.contNew.Line_repOps].const_cost;

        };

        function setISP() {
            console.log('curentCont.Isp:%o', $scope.curentCont.Isp);
            if ($scope.curentCont.Isp) {
                $scope.newCont.isp.svId = $scope.curentCont.Isp.sv_id;
                $scope.newCont.isp.svname = $scope.curentCont.Isp.svname;
                $scope.newCont.isp.monthlyPrice = $scope.curentCont.Isp.monthly_price[$scope.contSel.build_type];
            };
            console.log('newCont.isp.monthlyPrice:%o', $scope.newCont.isp);
        };

        function setTelCont() {
            $scope.newCont.tel.svId = $scope.curentCont.Tel.sv_id;
            $scope.newCont.tel.svname = $scope.curentCont.Tel.svname;
            $scope.newCont.tel.telType = $scope.curentCont.Tel.tel_type;
            $scope.newCont.tel.monthlyPrice = $scope.curentCont.Tel.conv_type;
            if (!($scope.contSel.tel.nums)) {
                $scope.contSel.tel.nums = $scope.curentCont.Tel.min_nums;
            };
            $scope.newCont.tel.nums = $scope.contSel.tel.nums;
            if (!($scope.contSel.tel.chs)) {
                $scope.contSel.tel.chs = $scope.curentCont.Tel.min_ch;
            };
            $scope.newCont.tel.chs = $scope.contSel.tel.chs;
            $scope.newCont.tel.numsCost = $scope.contSel.tel.nums * $scope.curentCont.Tel.nums_price;
            $scope.newCont.tel.chsCost = $scope.contSel.tel.chs * $scope.curentCont.Tel.ch_price;
            $scope.newCont.tel.options.gd = $scope.contSel.tel.hikari_ops.gd;
            $scope.newCont.tel.options.nd = $scope.contSel.tel.hikari_ops.nd;
            $scope.newCont.tel.options.nr = $scope.contSel.tel.hikari_ops.nr;
            $scope.newCont.tel.options.vw = $scope.contSel.tel.hikari_ops.vw;
            $scope.newCont.tel.options.vw_nums = $scope.contSel.tel.hikari_ops.vw_nums;
            $scope.newCont.tel.options.ro = $scope.contSel.tel.hikari_ops.ro;
            $scope.newCont.tel.options.rm = $scope.contSel.tel.hikari_ops.rm;
            $scope.newCont.tel.options.rm_nums = $scope.contSel.tel.hikari_ops.rm_nums;
            $scope.newCont.tel.options.fm = $scope.contSel.tel.hikari_ops.fm;
            $scope.newCont.tel.options.fm_nums = $scope.contSel.tel.hikari_ops.fm_nums;
            $scope.newCont.tel.options.rp = $scope.contSel.tel.hikari_ops.rp;
            console.log('newCont.tel.options:%o', $scope.newCont.tel.options);
            console.log('contSel.tel.hikari_ops:%o', $scope.contSel.tel.hikari_ops);
            

        }

        //  電話回線を変えた時に回線数／チャネル数をリセット
        $scope.onChOldtel = function () {
            $scope.contSel.tel.nums = $scope.curentCont.Tel.min_nums;
            $scope.contSel.tel.chs = $scope.curentCont.Tel.min_ch;
        };

        $scope.onChNumsChs = function () {
            if ($scope.curentCont.Tel.tel_type == 'ip') {
                setTelCont();
            };
        };

        function showObj() {
            console.log('contSel:%o', $scope.contSel);
            console.log('contNew:%o', $scope.contNew);
            console.log('newCont:%o', $scope.newCont);
        };

        // 計算の開始
        /*
        $scope.$watchGroup(['contSel','curentCont','contNew'], function (newValue, oldValue, scope) {
            console.log('curentCont:%o',$scope.curentCont);
            console.log('contSel:%o',$scope.contSel);
            console.log('newCont:%o',$scope.newCont);

            // 既存ネットワーク契約の判定
            setMenuType();
            // プロバイダの設定（既存の引継）
            setISP();

            // 回線の設定
            if ($scope.contNew.Line) {
                setLineCont()
            };


            // ひかり電話の設定
            if ($scope.contSel.tel && $scope.contSel.tel.tel_type == 'ip') {
                $scope.newCont.tel = $scope.contSel.tel;
            };


            // ひかり電話の回線数計算
            if ($scope.newCont.tel) {
                $scope.newCont.tel.numsCost = $scope.newCont.tel.hikariNums * $scope.newCont.tel.nums_price;
            };
            console.log($scope.newCont);
            showObj();
        });
*/
        // 既存回線の変更監視
        $scope.$watchCollection('curentCont.Line', function (newValue, oldValue, scope) {
            setMenuType();
        });

        // プロバイダの選択
        $scope.$watchCollection('curentCont.Isp', function (newValue, oldValue, scope) {
            setISP();
        });

        // 電話の選択
        $scope.$watchCollection('curentCont.Tel', function (newValue, oldValue, scope) {
            if ($scope.curentCont.Tel.tel_type == 'ip') {
                setTelCont();
            };

            showObj();
        });

        /*
        $scope.$watchCollection('contNew', function (newValue, oldValue, scope) {
            if ($scope.contNew.Line) {
                setLineCont()
            };

            showObj();
        });

//        $scope.$watchCollection('curentCont', function (newValue, oldValue, scope) {
//            setMenuType();
//            setISP();
//
        });
    */

    }])